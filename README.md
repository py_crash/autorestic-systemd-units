# Autorestic Systemd Units

Sample units for using autorestic with systemd-timers.

This project provides provide two `*.service` files:

* `autorestic-backup.service`: which runs the a backup command (`autorestic backup --all`) and a forget command
(`autorestic forget --all`), that marks the files for deletion.
* `autorestic-prune.service`: runs the "prune" command (`autorestic forget --all --prune`),that deletes the files from
the backends.

There are also two `*.timer` files. One for trigering the backup service daily, and one for running the prune service
once a month.

## Instalation

This systemd units could be installed system-wide, as a "system" unit, or only for your user, as a user unit.
You will need [restic] and [autorestic] already installed in your system.

For both instalations we start cloning the repo and going into the directory:

```
$ git clone https://gitlab.com/py_crash/autorestic-systemd-units.git
$ cd autorestic-systemd-units
```

Before continuing with the installition I would recommend that you check the files and modify them if you need it.

### As a system unit

For installing these units as **system units** you will need to copy them into the proper folder:

```
$ sudo cp autorestic-backup.service autorestic-backup.timer autorestic-prune.service autorestic-prune.timer /usr/lib/systemd/system/
```

The units will run as **root** and the autorestic configuration should be found in `/root/.autorestic.yml`. Please
be sure that you have a proper configuration and run `sudo autorestic check` at least once before continuing.

Now you could run them as a one time service:

```bash
$ systemctl start autorestic-backup.service  # For running the backup
$ systemctl start autorestic-prune.service  # For prunning
```

Or if you want this units to run automatically following the schedule (daily for backup and monthly for prune),
just activate the timers:

```bash
$ systemctl enable --now autorestic-backup.timer
$ systemctl enable --now autorestic-prune.timer
```

### As an user unit

If you don't want autorestic running as root, or you want to keep your configuration in your home folder, you could user
user units instead of system units.

As before, write a configuration file for autorestic, and place it in your home folder (`~/.autorestic.yml`) and run
`autorestic check` to make sure that everything works.

Then move this units to the proper folder (you may need to create it before you can place it there):

```bash
$ mkdir -p ~/.config/systemd/{,user}  # We make sure that the directory exists
$ cp autorestic-backup.service autorestic-backup.timer autorestic-prune.service autorestic-prune.timer ~/.config/systemd/user
```

Now you could run them as a one time service:

```bash
$ systemctl start --user autorestic-backup.service  # For running the backup
$ systemctl start --user autorestic-prune.service  # For prunning
```

Or if you want this units to run automatically following the schedule (daily for backup and monthly for prune),
just activate the timers:

```bash
$ systemctl enable --now --user autorestic-backup.timer
$ systemctl enable --now --user autorestic-prune.timer
```

## Licence

[GNU GPL v3](https://choosealicense.com/licenses/gpl-3.0/)

## Contributing

Found a bug? Want to add a feature? Contributions are always welcome!

Just send a Merge request on [GitLab](https://gitlab.com/py_crash/autorestic-systemd-units/-/merge_requests)

## TODO

- [X] Write a good README.
- [ ] Write a blog post about how I'm using autorestic.
- [ ] Create an installer that checks that `autorestic` and `restic` exists and uses flags for installing as a system or
user unit.


## Acknowledgements

 - The [autorestic] and [restic] projects that allow me to keep my data safe.
 - [Alex Kretzschmar][ktz] (aka @IronicBadger) from the [Self-Hosted podcast][ssh] talked about autorestic as a cool
 tool for creating backups. Check that [episode].
 - [Link Dupont][link] for his article on [Fedora Magazine][fedora], who inspired this project.

<!-- Links -->
[restic]: https://restic.net/
[autorestic]: https://autorestic.vercel.app/installation
[link]: https://fedoramagazine.org/author/linkdupont/
[fedora]: https://fedoramagazine.org/automate-backups-with-restic-and-systemd/
[ktz]: https://blog.ktz.me/
[ssh]: https://selfhosted.show/
[episode]: https://selfhosted.show/43